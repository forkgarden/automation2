import { Flow } from "../main/Flow";
import { BaseReusable } from "../main/BaseReusable";
import { page } from "../data/Page";

export class RLogout extends BaseReusable {
    run(flow: Flow, opts:Array<any> = []): void {
        flow
            .comment("Logout")
            .delay(5000)
            .click(page.homePage.btnLogout)
            .click(page.homePage.dialogConfirmLogout.btnComfirm)
            .checkElementLocated(page.logoutPage.btnLoginAgain);
    }
}