export namespace page {
    export const agentPage = {
        contextMenu: {
            btnIdentifyCustomer: { name: "button identify customer", xpath: "//a[contains(.,'Identify Customer')][1]" },
            btnCustomerProfile: { name: "" },
            btnCreateCase: { name: "" },
            btnRequestAgentChat: { name: "" },
            btnCobrowse: { name: "" },
            btnCreateHelpDeskCase: { name: "" },
            btnHandleCall: { name: "" },
            btnHandleWhiteMail: { name: "" },
            btnMakeCall: { name: "" },
            btnNewCustomer: { name: "" },
            btnReports: { name: "" },
            btnScriptDiscovery: { name: "" },
            btnSearchCase: { name: "" },
            btnSearchContactHistory: { name: "" },
            btnWarpUp: { name: "btn warp up", css: "button[id$=btnWrapup]" },

            selectedCustomer: {
                btnEditCustomer: {},
                btnCustomerProfile: {},
                btnEmail: {},
                btnCall: {},
                btnAssociation: {},
                btnContactHistory: {},
                btnCustomerCallback: {},
                btnSendWhiteMail: {}
            }
        },

        IdentifyCustomer: {

            inputFirstName: { name: "input first name", css: "input[id$=txtFirstName][type=text][name$=txtFirstName]" },
            inputLastName: { name: "input last name", css: "input[id$=txtLastName].GTTextField.txtLastName[type=text]" },
            inputDOB: {},
            inputPostalCode: {},
            inputTelpNo: {},
            inputEmail: {},
            inputCaseID: {},
            inputTwitterID: {},
            inputFacebookID: {},

            btnClear: {},
            btnSearch: { name: "btn search", css: "button[id$=btnSearch][name=gtxReserved_noJavaScript_actionString].text-in-center" },

            inputFilter: {},
            listSearchResult: {
                firstRow: { name: "list first row", css: 'table tbody.yui-dt-data tr:nth-child(1) td' },
                secondRow: { name: "list second row", css: 'table tbody.yui-dt-data tr:nth-child(2) td' },
                thirdRow: { name: "list third row", css: 'table tbody.yui-dt-data tr:nth-child(3) td' },
            },
            btnView: {},
            btnNoDetail: {},
            btnNewCustomer: {},
            btnSelect: { name: "btn select", css: "button[id$=btnSelect][name=gtxReserved_noJavaScript_actionString]" },
        },

        CustomerProfile: {
            btnTabSummary: { name: "tab summary", css: "" },
            btnTabCase: { name: "tab case", css: "" },
            tabSummary: {},
            tabCases: {
                inputFilter: {},
                listCases: {},
                btnOpen: {}
            },

            btnWrongCustomer: { name: "btn wrong customer", css: "" },
        },

        ListOfPendingCallback: {
            listPendingCallback: {},
            btnTakeWork: {},
            btnViewEdit: {},
            btnAdd: {}
        },

        CreateCallBack:{

        },

        CreateCase: {},
        RequestAgentChat: {},
        Cobrowse: {},
        CreateHelpDeskCase: {},
        HandleCall: {},
        HandleWhiteMail: {},
        MakeCall: {},
        NewCustomer: {},
        Reports: {},
        ScriptDiscovery: {},
        SearchCase: {},
        SearchContactHistory: {},

        WarpUp: {
            optReasonCode: {
                completed: { name: "reason code", css: "select[id$=optReasonCodes].GTOptionMenu[name$=optReasonCodes] option[value='Completed']" }
            },
            textAreaNote: { name: "note", css: "div.notes-textcontainter textarea.note-input" },
            btnConfirm: { name: "confirm", css: "button[id$=btnConfirm]" }
        },

        WorkList:{}

    }
}