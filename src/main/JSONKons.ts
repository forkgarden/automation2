import { BaseStep } from "./BaseStep";
import { PomStructure } from "./PomStructure";
import { BaseReusable } from "./BaseReusable";

// export class JSONKons extends BaseStep {
// 	static readonly GETTIME: string = 'getTime';

// 	run() {

// 	}
// }

export interface FlowData {
	_cmd: string;
	_el?: PomStructure;
	_data?: Array<any>;
	step?: BaseStep;
	reusable?: BaseReusable;
}

export class StepType {
    static readonly COMMENT: string = 'comment';
    static readonly OPEN_BROWSER: string = 'open browser';
    static readonly NAVIGATE: string = 'navigate';
    static readonly COMBINE: string = 'combine';
    static readonly RUNOTHERTEST: string = 'run other test';

    static readonly MAXIMIZE: string = 'maximize';
    static readonly SWTICH_TO_FRAME: string = 'switch to frame';
    static readonly RUNJS: string = 'runjs';
    static readonly CLICK: string = 'click';
    static readonly SEND_KEY: string = 'send key';

    static readonly WRITE: string = 'write';
    static readonly BROWSER_SWITCH: string = 'browser switch';
    static readonly DELAY: string = 'delay';
    static readonly DELAY_SEC: string = 'delay sec';
    static readonly DELAY_MIN: string = 'delay min';
    static readonly QUIT: string = 'quit';
    static readonly ERROR: string = 'error';

    static readonly VALIDATE_ELEMENT_ENABLE: string = 'validate element enabled';
    static readonly VALIDATE_ELEMENT_VISIBLE: string = 'validate element visible';
    static readonly VALIDATE_ELEMENT_LOCATED: string = 'validate element located';

}

/*
export enum __EnumStep {
	COMMENT,
	OPEN_BROWSER,
	NAVIGATE,
	COMBINE,
	RUNOTHERTEST,

	MAXIMIZE,
	SWTICH_TO_FRAME,
	RUNJS,
	CLICK,
	SEND_KEY,

	WRITE,
	BROWSER_SWITCH,
	DELAY,
	DELAY_SEC,
	DELAY_MIN,
	QUIT,
	ERROR,

	VALIDATE_ELEMENT_ENABLE,
	VALIDATE_ELEMENT_VISIBLE,
	VALIDATE_ELEMENT_LOCATED,

}
    // getTime()

    // write(pomParam: string | PomStructure, str: string = ""): Flow { return this.impl.write(pomParam, str); }

    // browserOpen(browser: string = 'chrome'): Flow { return this.impl.browserOpen(browser); }

    // browserSwitch(browserIdx: number = 0): Flow { return this.impl.browserSwitch(browserIdx); }

    // delay(n: number): Flow { return this.impl.delay(n); }

    // delaySec(n: number): Flow { return this.impl.delaySec(n); }

    // delayMin(n: number): Flow { return this.impl.delayMin(n); }

    // quit(): Flow { return this.impl.quit(); }

    // setOptions(opt?: FlowOption): Flow { return this.impl.setOptions(opt); }

    // error(opt?: ErrOption): Flow { return this.impl.error(opt); }



    // option(opt: any)

    // getPromise()

    // getSelenium()

    // comment(msg: String)

    // navigate(url: string)

    // combine(reusable: BaseReusable, opts: Array<any> = [])

    // runOtherTest(other: BaseStep)

    // checkElementEnable(pomParam: string | PomStructure)

    // checkElementVisible(pomParam: string | PomStructure)

    // checkElementLocated(pomParam: string | PomStructure): Flow { return this.impl.checkElementLocated(pomParam); }

    // maximize(): Flow { return this.impl.maximize(); }

    // switchTo(idx: number): Flow { return this.impl.switchTo(idx); }

    // runJs(js: Function): Flow { return this.impl.runJs(js); }

    // hideMask(): Flow { return this.impl.hideMask(); }

    // hideLoading(): Flow { return this.impl.hideLoading(); }

    // killSession(): Flow { return this.impl.killSession(); }

    // login(userName: string = "cccagent", password: string = "cccagent"): Flow { return this.impl.login(userName, password); }

    // click(pomParam: string | PomStructure): Flow { return this.impl.click(pomParam); }

    // sendKey(pomParam: string | PomStructure, key: string = Key.ENTER): Flow { return this.impl.sendKey(pomParam, key); }
*/

