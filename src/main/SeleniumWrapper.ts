import webdriver, { Capabilities, Key, By} from 'selenium-webdriver';
import http from 'selenium-webdriver/http';
import { BaseStep } from './BaseStep';

export class SeleniumWrapper {
	private until = webdriver.until; c
	private driver!: webdriver.WebDriver;
	private driverList: Array<webdriver.WebDriver> = [];
	private _lastEl: webdriver.WebElement;
	private driverId:string = "";
	

	static create(): SeleniumWrapper {
		return new SeleniumWrapper();
	}

	constructor() {

	}

	async createBrowserChromeFromId(url:string):Promise<any> {
		let executer:http.Executor = new http.Executor(
			Promise.resolve(url).then(url => new http.HttpClient(url, null, null)
		));

		let session:webdriver.Session = new webdriver.Session(
			this.driverId,
			executer
		);

		this.driver = new webdriver.WebDriver(session, executer);
		this.driverList.push(this.driver);
	}

	async createBrowserChrome(): Promise<any> {
		this.driver = new webdriver.Builder()
			.forBrowser('chrome')
			.withCapabilities(Capabilities.chrome().set("acceptInsecureCerts", true).setAlertBehavior('dismiss'))
			.build();

		this.driverId = (await (await this.driver).getSession()).getId();

		//webdriver.

		this.driverList.push(this.driver);
	}

	createBrowserFirefox(): webdriver.WebDriver {
		this.driver = new webdriver.Builder()
			.forBrowser('firefox')
			.withCapabilities(Capabilities.firefox().set("acceptInsecureCerts", true).setAlertBehavior('dismiss'))
			.build();

		this.driverList.push(this.driver);
		return this.driver;
	}

	getCurrentDriver(): webdriver.WebDriver {
		return this.driver;
	}

	switchDriverByIdx(idx: number): void {
		this.driver = this.driverList[idx];

		console.log("swicht driver, idx " + idx + "/total " + this.driverList.length);
	}

	async waitTime(n: number): Promise<any> {
		return new Promise<any>((resolve, reject) => {
			setTimeout(() => {
				let date: Date = new Date();
				resolve();
			}, n);
		});
	}

	async switchTo(idx: number): Promise<void> {
		await this.driver.switchTo().frame(idx);
	}

	async navigate(url: string): Promise<void> {
		await this.driver.get(url);
	}

	async write(locator: webdriver.Locator, str: string): Promise<void> {
		await this.driver.findElement(locator).sendKeys(str);
	}

	async waitElementInvisible(locator: webdriver.Locator, timeOut: number): Promise<any> {
		console.log("wait elemetn invisible");
		let el: any = await this.driver.wait(this.until.elementLocated(locator), timeOut);
		await this.driver.wait(this.until.elementIsNotVisible(el), timeOut);
	}

	async waitElementEnable(locator: webdriver.Locator, timeOut: number): Promise<any> {
		let el: any = await this.driver.wait(this.until.elementsLocated(locator), timeOut);
		await this.driver.wait(this.until.elementIsEnabled(el[0]));
	}

	async waitElementVisible(locator: webdriver.Locator, timeOut: number): Promise<any> {
		let el: any = await this.driver.wait(this.until.elementsLocated(locator), timeOut);
		await this.driver.wait(this.until.elementIsVisible(el[0]), timeOut);
	}

	async waitElement(locator: webdriver.Locator, timeOut: number): Promise<any> {
		await this.driver.wait(this.until.elementsLocated(locator), timeOut);
	}

	async runOther(other: BaseStep): Promise<any> {
		other.run();
		let p: Promise<any> = other.getPromise();
		await p;
		console.log("process done " + p);
	}

	async click(locator: webdriver.Locator, timeOut: number): Promise<void> {
		let loading: boolean = await this.getCurrentDriver().executeScript(() => {
			let el: HTMLElement = document.querySelector("div#GTMainGlass.GTInvisibleGlass");

			if (!el) return false;

			let display = el.style.display;

			return display == 'block';
		});

		if (loading) {
			console.log("loading available, wait for loading tobe invisible");
			await this.waitElementInvisible(By.css("div#GTMainGlass.GTInvisibleGlass"), timeOut);
		}

		await this.driver.findElement(locator).click();
	}

	async quit(): Promise<void> {
		console.log("quit " + this.driver);
		await this.driver.quit();
	}

	public get lastEl(): webdriver.WebElement {
		return this._lastEl;
	}

}