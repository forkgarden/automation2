import { FlowData, StepType } from "../main/JSONKons"
import { BaseStep } from "../main/BaseStep";

export class GoogleTest2 extends BaseStep {
	private data: Array<FlowData>;

	constructor() {
		super();
		this.data = [
			{
				_cmd: StepType.OPEN_BROWSER
			},
			{
				_cmd: StepType.NAVIGATE,
				_data: ["http://www.google.com"]
			},
			{
				_cmd: StepType.WRITE,
				_el: { css: "input.gLFyf.gsfi", name: "search input" },
				_data: ["automation for dummies"]
			},
			{
				_cmd: StepType.SEND_KEY,
				_el: { css: "input.gLFyf.gsfi", name: "search input" }
			},
			{
				_cmd: StepType.DELAY,
				_data: [5000]
			},
			{
				_cmd: StepType.QUIT
			}
		]
	}

	run() {
		this.runFromData(this.data);
	}
}
