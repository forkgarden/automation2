import { BaseStep } from "../main/BaseStep"

export class Matematika extends BaseStep {

	run() {
		//write your steps here, you can start it with
		//this.flow.browserOpen().maximize().navigate(Url.Ad)
		this.flow.browserOpen().navigate("http://localhost/git/proto/ulangan/banyakan/web/")

			.comment('Menghitung jumlah benda')
			.click("ul li:nth-child(1) button:nth-child(1)")
			.click("ul li:nth-child(1) li button:nth-child(1)")
			.click('button.tutup')
			.click("ul li:nth-child(1) button:nth-child(1)")

			.comment('Membandingkan')
			.click("ul li:nth-child(2) button:nth-child(1)")
			.click("ul li:nth-child(2) li button:nth-child(1)")
			.click("div.cont div.banyakan button.tutup")
			.click("ul li:nth-child(2) button:nth-child(1)")

			.comment('Membandingkan 2')
			.click("ul li:nth-child(2) button:nth-child(1)")
			.click("ul li:nth-child(2) li:nth-child(2) button:nth-child(1)")
			.click("div.cont div.banyakan button.tutup")
			.click("ul li:nth-child(2) button:nth-child(1)")

			.comment('Membandingkan 3')
			.click("ul li:nth-child(2) button:nth-child(1)")
			.click("ul li:nth-child(2) li:nth-child(3) button:nth-child(1)")
			.click("div.cont div.banyakan button.tutup")
			.click("ul li:nth-child(2) button:nth-child(1)")

			.comment('Membandingkan 4')
			.click("ul li:nth-child(2) button:nth-child(1)")
			.click("ul li:nth-child(2) li:nth-child(4) button:nth-child(1)")
			.click("div.cont div.banyakan button.tutup")
			.click("ul li:nth-child(2) button:nth-child(1)")

			.comment('Membandingkan 5')
			.click("ul li:nth-child(2) button:nth-child(1)")
			.click("ul li:nth-child(2) li:nth-child(5) button:nth-child(1)")
			.click("div.cont div.banyakan button.tutup")
			.click("ul li:nth-child(2) button:nth-child(1)")

			.delay(5000)
			.error()
	}
}
