import { page } from "../data/Page";
import { Url } from "../data/Page";
import { BaseStep } from "../main/BaseStep"

export class TestPage extends BaseStep {

    run() {
        this.openEMQA();
        this.login();
        this.validateContextMenu();
        this.flow.click(page.agentPage.contextMenu.btnIdentifyCustomer).delaySec(5);
        this.validateIdentifyCustomerPage();
        this.logout();

    }

    validateIdentifyCustomerPage(): void {
        this.flow
            .checkElementLocated("//input[contains(@id,'_txtFirstName')][1]")
            .checkElementLocated("//input[contains(@id,'_txtLastName')][1]")
            .checkElementLocated("//input[contains(@id,'_datDateofbirth')][1]")
            .checkElementLocated("//input[contains(@id,'_txtPostalcode')][1]")
            .checkElementLocated("//input[contains(@id,'_txtTelephonenumber')][1]")
            .checkElementLocated("//input[contains(@id,'_txtEmailaddress')][1]")
            .checkElementLocated("//input[contains(@id,'_txtCaseId')][1]")
            .checkElementLocated("//input[contains(@id,'_txtTwitterID')][1]")
            .checkElementLocated("//input[contains(@id,'_txtFacebooKID')][1]")
            .checkElementLocated("//button[contains(@id,'_btnClear')][1]")
            .checkElementLocated("//button[contains(@id,'_btnSearch')][contains(.,'Search')][1]")
            .checkElementLocated("//button[contains(@id,'_btnClear')][1]")
            .click("//div[contains(@class, 'tab')][contains(., 'Offline')][contains(@class, 'currentTab')]//input")
            .delaySec(5)
            .hideLoading()
            .click("//button[contains(@id,'_btnConfirm')]")
            .delaySec(5)
            .hideLoading()
        //this.warpUp();
    }

    validateContextMenu(): void {
        this.flow
            .checkElementLocated("//a[contains(.,'Agent Chat')][1]")     //contextMenu -> agentChat
            .checkElementLocated("//a[contains(.,'Cobrowse')][1]")       //contextMenu -> cobrowse
            .checkElementLocated("//a[contains(.,'Create Case')][1]")        //contextMenu -> createCase
            .checkElementLocated("//a[contains(.,'Create HelpDesk Case')][1]") //contextMenu -> crateHelpDeskCase
            .checkElementLocated("//a[contains(.,'Handle Call')][1]") //contextMenu -> handleCall
            .checkElementLocated("//a[contains(.,'Handle Whitemail')][1]") //contextMenu -> handleWhiteMail
            .checkElementLocated("//a[contains(.,'Identify Customer')][1]") //contextMenu -> identifyCustomer
            .checkElementLocated("//a[contains(.,'Make Call')][1]") //contextMenu -> makeCall
            .checkElementLocated("//a[contains(.,'New Customer')][1]") //contextMenu -> newCustomer
            .checkElementLocated("//a[contains(.,'Reports')][1]") //contextMenu -> reports
            .checkElementLocated("//a[contains(.,'Script Discovery')][1]") //contextMenu -> scriptDiscovery
            .checkElementLocated("//a[contains(.,'Search Cases')][1]") //contextMenu -> searchCases
            .checkElementLocated("//a[contains(.,'Search Contact History')][1]") //contextMenu -> searchContactHistory
    }

    openEMQA(): void {
        this.flow
            .browserOpen("firefox")
            .maximize()
            .navigate(Url.AdMahameruQA)
    }

    login(): void {
        this.flow
            .comment("Login to EM")
            .write(page.loginPage.inputUserName, "cccagent")
            .write(page.loginPage.inputPassword, "cccagent")
            .click(page.loginPage.btnLogin)
            .delaySec(10)
    }

    logout(): void {
        this.flow
            .comment("Logout")
            .delay(5000)
            .hideLoading()
            .click(page.homePage.btnLogout)
            .click(page.homePage.dialogConfirmLogout.btnComfirm)
            .checkElementLocated(page.logoutPage.btnLoginAgain)
            .quit();
    }

    warpUp(): void {
        this.flow
            .comment("Warp Up Customer")
            .delay(5000)
            .click(page.agentPage.contextMenu.btnWarpUp)
            .click(page.agentPage.WarpUp.optReasonCode.completed)
            .write(page.agentPage.WarpUp.textAreaNote, "note")
            .click(page.agentPage.WarpUp.btnConfirm)
    }
}
