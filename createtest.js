var fs = require('fs');

const args = process.argv;

var content2 = '' +
    'export class ' + args[2] + ' extends BaseStep {\n' +
    '\n' +
    '    run() {\n' +
    '       //write your steps here, you can start it with\n' +
    '       //this.flow.browserOpen().maximize().navigate(Url.Ad)\n' +
    '    }\n' +
    '}\n';


console.log(args);

if (args.length > 2) {
    var importText = '' +
    'import { page } from "../../data/Page";\n' +
    'import { Url } from "../../data/Page";\n' +
    'import { BaseStep } from "../../main/BaseStep"\n' +
    '\n';

    fs.writeFile("src/steps/" + args[3] + "/" + args[2] + ".ts", importText + content2, () => {});
}
else {
    var importText = '' +
    'import { page } from "../data/Page";\n' +
    'import { Url } from "../data/Page";\n' +
    'import { BaseStep } from "../main/BaseStep"\n' +
    '\n';

    fs.writeFile("src/steps/" + args[2] + ".ts", importText + content2, () => { });
}
